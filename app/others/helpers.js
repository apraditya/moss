(function() {

  'use strict';

  // Protects views where angular is not loaded from errors
  if ( typeof angular === 'undefined' ) {
    return;
  }

  var module = angular.module('AppHelpers', []);

  module.factory('Helpers', function() {
    return {

      decodeHTML: function(encodedHTML) {
        // strip string literals
        var stripLiterals = function(aString) {

          var literalRegexes = [/\b/g, /\f/g, /\n/g, /\r/g, /\t/g];

          _.each(literalRegexes, function(regex) {
            var strLiteral = regex.toString();
            strLiteral = strLiteral.substr(1, strLiteral.length-3);
            aString = aString.replace(regex, '');
            if (aString[aString.length-1] === strLiteral) {
              aString = aString.substr(0, aString.length-1);
            }
          });

          return aString;
        };

        return window.unescape(JSON.parse('"' + stripLiterals(encodedHTML) + '"'));
      },

      removeLinkRef: function(decodedHTML) {
        var div = document.createElement('div');
        div.innerHTML = decodedHTML;

        _.each(div.getElementsByTagName('a'), function(linkElement) {
          linkElement.removeAttribute('href');
        });

        return div.innerHTML;
      },

      sanitize: function(encodedHTML) {
        return this.removeLinkRef(this.decodeHTML(encodedHTML));
      }
    };
  });


})();
