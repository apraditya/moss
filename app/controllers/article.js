var articleApp = angular.module('articleApp', [
    'ngRoute', 'ngSanitize', 'AppHelpers',
    'ArticleModel', 'hmTouchevents'
  ]);

// Index: http://localhost/views/article/index.html

articleApp.controller('IndexCtrl', function ($scope, Helpers, ArticleRestangular) {

  // This will be populated with Restangular
  $scope.articles = [];

  // Helper function for opening new webviews
  $scope.open = function(id) {
    webView = new steroids.views.WebView("/views/article/show.html?id="+id);
    steroids.layers.push(webView);
  };

  // Helper function for loading article data with spinner
  $scope.loadArticles = function() {
    $scope.loading = true;

    articles.getList().then(function(data) {
      _.each(data, function(article) {
        article.summary = Helpers.decodeHTML(article.summary);
      });

      $scope.articles = data;
      $scope.loading = false;
    });

  };

  // Fetch all objects from the backend (see app/models/article.js)
  var articles = ArticleRestangular.all('articles');
  $scope.loadArticles();


  // Get notified when an another webview modifies the data and reload
  window.addEventListener("message", function(event) {
    // reload data on message with reload status
    if (event.data.status === "reload") {
      $scope.loadArticles();
    };
  });


  // -- Native navigation

  // Set navigation bar..
  steroids.view.navigationBar.show("Artikel");


});


// Show: http://localhost/views/article/show.html?id=<id>

articleApp.controller('ShowCtrl', function ($scope, Helpers, ArticleRestangular) {

  // Set navigation bar
  $scope.setNavigationBar = function(navigationBar) {
    steroids.view.navigationBar.show(navigationBar);
  };

  // Helper function for loading article data with spinner
  $scope.loadArticle = function() {
    $scope.loading = true;

    article.get().then(function(data) {
      data['readableContent'] = Helpers.sanitize(data['readableContent']);
      $scope.article = data;

      $scope.setNavigationBar($scope.article.title);
      $scope.loading = false;
    });

  };

  var article = ArticleRestangular.one("articles", steroids.view.params.id);
  $scope.loadArticle();


});
