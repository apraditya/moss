// The contents of individual model .js files will be concatenated into dist/models.js

(function() {

// Protects views where angular is not loaded from errors
if ( typeof angular == 'undefined' ) {
	return;
};


var module = angular.module('ArticleModel', ['restangular']);

module.factory('ArticleRestangular', function(Restangular) {

  return Restangular.withConfig(function(RestangularConfigurer) {

    RestangularConfigurer.setBaseUrl('http://api.situssunnah.com/api/v1/');
    RestangularConfigurer.setRequestSuffix('.json');

  });

});


})();
